# !/bin/bash
docker build -t alex/reverse-proxy ../reverse_proxy
docker push alex/reverse-proxy:v1
docker build -t alex/db-postgresql ../db
docker push alex/db-postgresql:v1