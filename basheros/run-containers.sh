# !/bin/bash
docker network create my-net
docker run -d -p80:80 --name reverse-proxy \
  --network my-net \
  alex/reverse-proxy
mvn compile jib:dockerBuild
docker run -d -p80:8000 --name backend-app \
  --network my-net \
  alex/backend-app